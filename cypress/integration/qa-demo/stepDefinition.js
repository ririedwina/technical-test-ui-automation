import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

const verify = ["ISBN", "9781449325862", "Title", "Git Pocket Guide", "Sub Title", "A Working Introduction",
"Author", "Richard E. Silverman", "Publisher", "O'Reilly Media", "Total Pages", "234", "Description", 
"This pocket guide is the perfect on-the-job companion to Git, the distributed version control system. It provides a compact, readable introduction to Git for new users, as well as a reference to common commands and procedures for those of you with Git exp",
"Website", "http://chimera.labs.oreilly.com/books/1230000000561/index.html"]

Given("User go to {string}", url => {
   cy.visit(url)
});

When("User in {string} page", pageTitle => {
    cy.get(".main-header").contains(pageTitle)

});

And("User choose select value Another root option", () => {
    cy.get(".css-1wy0on6").eq(0).click({force: true})
    cy.get(".css-yt9ioa-option").eq(4).click({force: true})
});

And("User choose select one Other", () => {
    cy.get(".css-1wy0on6").eq(1).click({force: true})
    cy.get(".css-yt9ioa-option").eq(4).click({force: true})
});

And("User choose old style select menu Aqua", () => {
    cy.get('#oldSelectMenu').select(10)  
});

And("User choose multi select drop down all color", () => {
    cy.get(".css-1wy0on6").eq(2).click({force: true})
    cy.get('[id^=react-select-4-option]').click({ multiple: true })
});

Then("User success input all select menu", () => {
    cy.get(".css-1uccc91-singleValue").eq(0).contains('Another root option')
    cy.get(".css-1uccc91-singleValue").eq(1).contains('Other')
    cy.get(".css-1hwfws3").eq(2).contains('Green')
    cy.get(".css-1hwfws3").eq(2).contains('Blue')
    cy.get(".css-1hwfws3").eq(2).contains('Black')
    cy.get(".css-1hwfws3").eq(2).contains('Red')
});

And("User search book {string}", search => {
    cy.get('input').eq(0).type(search)
});

Then("User see No rows found", () => {
    cy.get(".rt-noData").should('be.visible')
});

Then("User click book Git Pocket Guide", () => {
    cy.get('a[href*="/books?book=9781449325862"]').invoke('removeAttr', 'target').click()
});

Then("User see Git Pocket Guide", () => {
    for (let index = 0; index < verify.length; index++) {
        cy.get(".form-label").eq(index).contains(verify[index])
        
    }
});